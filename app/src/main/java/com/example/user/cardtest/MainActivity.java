package com.example.user.cardtest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText mNameEditText;
    private EditText mSurnameEditText;
    private ImageView mEditImageView;
    private ImageView mAcceptImageView;
    private TextView mNameTextView;
    private TextView mSurnameTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final UserData user = new UserData();

        mNameEditText = (EditText)findViewById(R.id.name);
        mSurnameEditText = (EditText) findViewById(R.id.surname);
        mEditImageView = (ImageView) findViewById(R.id.edit);
        mAcceptImageView = (ImageView)findViewById(R.id.accept);
        mNameTextView = (TextView) findViewById(R.id.nameview);
        mNameTextView.setText(user.getName());
        mSurnameTextView = (TextView) findViewById(R.id.surnameview);
        mSurnameTextView.setText(user.getSurname());

        mEditImageView.setClickable(true);
        mEditImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mEditImageView.setVisibility(View.GONE);
                mAcceptImageView.setVisibility(View.VISIBLE);
                mNameTextView.setVisibility(View.GONE);
                mSurnameTextView.setVisibility(View.GONE);
                mNameEditText.setVisibility(View.VISIBLE);
                mSurnameEditText.setVisibility(View.VISIBLE);




            }
        });

        mAcceptImageView.setClickable(true);
        mAcceptImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAcceptImageView.setVisibility(View.GONE);
                mEditImageView.setVisibility(View.VISIBLE);
                mNameEditText.setVisibility(View.GONE);
                mSurnameEditText.setVisibility(View.GONE);
                mNameTextView.setVisibility(View.VISIBLE);
                mSurnameTextView.setVisibility(View.VISIBLE);

                user.setName(mNameEditText.getText().toString());
                user.setSurname(mSurnameEditText.getText().toString());
                mNameTextView.setText(user.getName());
                mSurnameTextView.setText(user.getSurname());

            }
        });

    }


}
